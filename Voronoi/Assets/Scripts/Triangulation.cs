﻿using System;
using System.Collections.Generic;
using UnityEngine;
using WordsOnPlay.Utils;

public class Triangulation {

    private Triangle[] root;

    private List<Vector2> vertices;

    public Vector2[] Vertices {
        get {
            return vertices.ToArray();
        }
    }

    public int[] Triangles {
        get {

            HashSet<Triangle> triangles = new HashSet<Triangle>();
            CollectTriangles(triangles, root[0]);
            CollectTriangles(triangles, root[1]);

            List<int> indices = new List<int>();

            foreach (Triangle t in triangles) {
                // clockwise order
                indices.Add(t.P(0));
                indices.Add(t.P(1));
                indices.Add(t.P(2));
            }

            return indices.ToArray();
        }
    }

    private void CollectTriangles(HashSet<Triangle> triangles, Triangle t) {
        if (t.children == null) {
            triangles.Add(t);
            return;
        }

        foreach (Triangle tc in t.children) {
            CollectTriangles(triangles, tc);
        }
    }

    private int nTriangles;
    public int Count {
        get {
            return nTriangles;
        }
    }


    public class Triangle {
        public RingArray<Triangle> children;
        public RingArray<Triangle> neighbours;
        private RingArray<int> p;
        private Triangulation outer;

        public Triangle(Triangulation outer, int p0, int p1, int p2) {
            this.outer = outer;
            this.neighbours = new RingArray<Triangle>(3);
            this.children = null;
            this.p = new RingArray<int>(3);
            this.p[0] = p0;
            this.p[1] = p1;
            this.p[2] = p2;
        }

        public Triangle Next(int i, bool clockwise) {
            
            if (!clockwise) {
                i = i - 1;
            }

            return this.neighbours[i];
        }

        public override string ToString()
        {
            return p.ToString();
        }

        public int P(int i) {
            return p[i];
        }

        public Vector2 V(int i) {
            return outer.vertices[p[i]];
        }

        public float Angle(int i) {
            Vector2 va = this.V(i + 1) - this.V(i);
            Vector2 vb = this.V(i - 1) - this.V(i);

            return Vector2.Angle(va, vb);
        }

        public bool Contains(Vector2 p) {
            return IsOnRight(p - V(0), V(1) - V(0)) && IsOnRight(p - V(1), V(2) - V(1)) && IsOnRight(p - V(2), V(0) - V(2));
        }

        public int FindNeighbour(Triangle t1) {

            for (int j = 0; j < 3; j++) {
                if (neighbours[j] == t1) {
                    return j;
                }
            }

            throw new System.Exception("Neighbour not found: " + t1);
        }

        public Triangle Next(int i) {
            for (int j = 0; j < 3; j++) {
                if (p[j] == i) {
                    return neighbours[j];
                }
            }
            
            throw new System.Exception("Vertex not found: " + p);
        }

        public void ReplaceNeighbour(Triangle oldNeighbour, Triangle newNeighbour) {
            int i = FindNeighbour(oldNeighbour);
            neighbours[i] = newNeighbour;
        }

        public void DrawGizmo() {
            for (int i = 0; i < 3; i++) {
                Gizmos.DrawLine(V(i), V(i+1));
            }

        }

        public Vector2 Circumcenter() {
            float x1 = V(1).x - V(0).x;
            float y1 = V(1).y - V(0).y;
            float x2 = V(2).x - V(0).x;
            float y2 = V(2).y - V(0).y;

            Vector2 c;

            float d = (x2 * y1 - x1 * y2) * 2;
            if (d == 0) {
                throw new System.Exception("Cannot take circumcenter of empty triangle.");
            }

            c.x = (y1 * (x2 * x2 + y2 * y2) - y2 * (x1 * x1 + y1 * y1)) / d;
            c.y = (x2 * (x1 * x1 + y1 * y1) - x1 * (x2 * x2 + y2 * y2)) / d; 

            return V(0) + c;
        }

        public float Area() {
            Vector2 v1 = V(1) - V(0);
            Vector2 v2 = V(2) - V(0);

            return Vector2Extensions.Cross(v1, v2) / 2;
        }

    }

	public static bool IsOnRight(Vector2 v1, Vector2 v2) {

        // if it is on the edge, it belongs to the triangle above
        // if the edge is vertrical, it belongs to the triangle on the right
        if (v1.x * v2.y == v1.y * v2.x) {
            if (v2.x == 0) {
                return v2.y > 0;
            }
            
            return v2.x > 0;
        }

		return v1.x * v2.y > v1.y * v2.x;
	}

    public Triangulation(Rect r) {
        vertices = new List<Vector2>();
        vertices.Add(r.Corner(0));
        vertices.Add(r.Corner(1));
        vertices.Add(r.Corner(2));
        vertices.Add(r.Corner(3));

        root = new Triangle[2];

        // clockwise
        root[0] = new Triangle(this, 2, 3, 0);
        root[1] = new Triangle(this, 0, 1, 2);

        root[0].neighbours[0] = root[1];
        root[1].neighbours[0] = root[0];

        nTriangles = 2;
    }

    internal List<Vector2> GetVoronoiTrangles(int i)
    {
        i = i + 4; // skip boundingbox
        List<Vector2> voronoi = new List<Vector2>();
        Vector2 p = vertices[i];      

        Triangle first = Contains(p);
        Triangle last = first;
        Vector2 lastC = last.Circumcenter();

        do {
            Triangle t = last.Next(i);
            Vector2 c = t.Circumcenter();

            voronoi.Add(p);
            voronoi.Add(lastC);
            voronoi.Add(c);

            last = t;
            lastC = c;
        }
        while (first != last);
        
        return voronoi;
    }

    public void AddPoint(Vector2 point) {
        Triangle t = Contains(point);
        vertices.Add(point);
        int p = vertices.Count - 1;

        nTriangles += 3;
        t.children = new RingArray<Triangle>(3);
        for (int i = 0; i < 3; i++) {
            t.children[i] = new Triangle(this, p, t.P(i), t.P(i+1));
        }

        // connect to neighbours
        for (int i = 0; i < 3; i++) {
            t.children[i].neighbours[0] = t.children[i+1];
            t.children[i].neighbours[1] = t.children[i+2];
            t.children[i].neighbours[2] = t.neighbours[i+1];

            if (t.neighbours[i+1] != null) {
                t.neighbours[i+1].ReplaceNeighbour(t, t.children[i]);
            }

        }

        // check any quads to maintain delaunay property 

        for (int i = 0; i < 3; i++) {
            CheckQuad(t.children[i], 0);
        }
    }

    private void CheckQuad(Triangle t1, int i) {

        i = i + 2;

        if (IsLegal(t1, i)) {
            return;
        }

        Triangle t2 = t1.neighbours[i];
        int j = t2.FindNeighbour(t1);

        Triangle ta = new Triangle(this, t1.P(i), t1.P(i+1), t2.P(j+1));
        Triangle tb = new Triangle(this, t2.P(j), t2.P(j+1), t1.P(i+1));
 
        ta.neighbours[0] = t2.neighbours[j+2];
        ta.neighbours[1] = t1.neighbours[i+1];
        ta.neighbours[2] = tb;        

        if (ta.neighbours[0] != null) {
            ta.neighbours[0].ReplaceNeighbour(t2, ta);
        }

        if (ta.neighbours[1] != null) {
            ta.neighbours[1].ReplaceNeighbour(t1, ta);
        }

        tb.neighbours[0] = t1.neighbours[i+2];
        tb.neighbours[1] = t2.neighbours[j+1];
        tb.neighbours[2] = ta;        

        if (tb.neighbours[0] != null) {
            tb.neighbours[0].ReplaceNeighbour(t1, tb);
        }

        if (tb.neighbours[1] != null) {
            tb.neighbours[1].ReplaceNeighbour(t2, tb);
        }

        t1.children = new RingArray<Triangle>(2);
        t1.children[0] = ta;
        t1.children[1] = tb;

        t2.children = new RingArray<Triangle>(2);
        t2.children[0] = ta;
        t2.children[1] = tb;

        // check next quad around each point
        CheckQuad(ta, 1);
        CheckQuad(tb, 2);   

    }

    private bool IsLegal(Triangle t1, int i) {
        Triangle t2 = t1.neighbours[i];

        if (t2 == null) {
            return true;
        }

        int j = t2.FindNeighbour(t1);

        return t1.Angle(i+1) + t2.Angle(j+1) <= 180;
    }

    public Triangle Contains(Vector2 p) {

        Triangle t;

        if (root[0].Contains(p)) {
            t = root[0];
        }
        else if (root[1].Contains(p)) {
            t = root[1];
        }
        else {
            return null;
        }

        while (t.children != null) {
            for (int i = 0; i < t.children.Length; i++) {
                if (t.children[i].Contains(p)){
                    t = t.children[i];
                    break;
                }
            }
        }

        return t;

    }

    public void DrawGizmo() {
        DrawGizmo(root[0]);
        DrawGizmo(root[1]);
    }

    public void DrawGizmo(Triangle t) {
        if (t.children != null) {
            for (int i = 0; i < t.children.Length; i++) {
                DrawGizmo(t.children[i]);
            }
        }
        else {
            t.DrawGizmo();
        }
    }
}
