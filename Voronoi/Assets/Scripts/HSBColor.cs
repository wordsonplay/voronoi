using UnityEngine;

/**
 * A utility for converting HSB colour to Unity Color.
 */

[System.Serializable]
public struct HSBColor {

	public static Color ToColor(float h, float s, float b, float a = 1.0f) {
		float red = b;
		float green = b;
		float blue = b;
		if (s != 0) {
			float max = b;
			float dif = b * s;
			float min = b - dif;
			
			h *= 360f;
			
			if (h < 60f) {
				red = max;
				green = h * dif / 60f + min;
				blue = min;
			} else if (h < 120f) {
				red = -(h - 120f) * dif / 60f + min;
				green = max;
				blue = min;
			} else if (h < 180f) {
				red = min;
				green = max;
				blue = (h - 120f) * dif / 60f + min;
			} else if (h < 240f) {
				red = min;
				green = -(h - 240f) * dif / 60f + min;
				blue = max;
			} else if (h < 300f) {
				red = (h - 240f) * dif / 60f + min;
				green = min;
				blue = max;
			} else if (h <= 360f) {
				red = max;
				green = min;
				blue = -(h - 360f) * dif / 60 + min;
			} else {
				red = 0;
				green = 0;
				blue = 0;
			}
		}
		
		return new Color(Mathf.Clamp01(red), Mathf.Clamp01(green), Mathf.Clamp01(blue), a);
	}
	    
}