﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Voronoi2;
using WordsOnPlay.Utils;

[RequireComponent(typeof(MeshFilter))]
public class DrawVoronoi : MonoBehaviour
{
    public Transform pointPrefab;
    private Transform selectedPoint;
    private int nPointsCreated = 0;

    public float hue = 0;
    public float hueIncrement = (Mathf.Sqrt(5.0f) - 1.0f) / 2.0f;
    public float saturation = 1.0f;
    public float brightness = 1.0f;

    public float voronoiErrorMargin = 0.1f;

    private Rect boundingRect;

    private Triangulation triangulation;
    private MeshFilter meshFilter;

    // Start is called before the first frame update
    void Start()
    {
        // assumes orthographic 2D camera along the Z axis
        Vector2 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector2 topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        Vector2 centre = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        Vector2 size = topRight - bottomLeft;

        transform.position = centre;

        boundingRect = new Rect(bottomLeft.x, bottomLeft.y, size.x, size.y);
        triangulation = new Triangulation(boundingRect);

        meshFilter = GetComponent<MeshFilter>();
        RecalculateMesh();
    }

    // Update is called once per frame
    void Update()
    {
        bool updateRequired = false;

        if (Input.GetMouseButtonDown(0))
        {
            Vector2 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            selectedPoint = GetPoint(point);

            if (selectedPoint == null)
            {
                nPointsCreated++;
                selectedPoint = Instantiate(pointPrefab);
                selectedPoint.parent = transform;
                selectedPoint.position = point;
                selectedPoint.gameObject.name = "Point " + nPointsCreated;

                Color color = HSBColor.ToColor(hue, saturation, brightness);

                SpriteRenderer sprite = selectedPoint.Find("Sprite").GetComponent<SpriteRenderer>();
                sprite.color = color;

                MeshRenderer mr = selectedPoint.Find("Voronoi").GetComponent<MeshRenderer>();
                mr.material.SetColor("_WireColor", color);

                color.a = 0.5f;
                mr.material.SetColor("_FillColor", color);

                hue = (hue + hueIncrement) % 1.0f;
                updateRequired = true;
            }
        }

        if (Input.GetMouseButton(0) && selectedPoint != null)
        {
            Vector2 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            selectedPoint.position = point;
            updateRequired = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            selectedPoint = null;
        }

        if (Input.GetMouseButtonDown(1) && selectedPoint == null)
        {
            Vector2 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Transform deletedPoint = GetPoint(point);

            if (deletedPoint != null)
            {
                // disconnect it, so the update works below
                // as Destroy won't happen until later
                deletedPoint.parent = null;
                Destroy(deletedPoint.gameObject);
                updateRequired = true;
            }
        }

        if (updateRequired)
        {
            triangulation = new Triangulation(boundingRect);

            for (int i = 0; i < transform.childCount; i++)
            {
                Transform p = transform.GetChild(i);
                triangulation.AddPoint(p.position);
            }

            RecalculateMesh();
            RecalculateVoronoiMesh();
        }

    }

    private void RecalculateMesh()
    {
        Mesh mesh = meshFilter.mesh;

        Vector2[] vertices2D = triangulation.Vertices;
        int[] triangles = triangulation.Triangles;

        // include each vertex multiple times so we can set different uvs

        int n = triangles.Length;
        Vector3[] vertices3D = new Vector3[n];
        Vector2[] uvs = new Vector2[n];

        for (int i = 0; i < triangles.Length; i++)
        {
            vertices3D[i] = vertices2D[triangles[i]];
            triangles[i] = i;

            // uvs are set to barycentric coords
            // the third coordinate is implied b2 = 1 - b0 - b1

            switch (i % 3)
            {
                case 0:
                    uvs[i] = new Vector2(0, 0);
                    break;
                case 1:
                    uvs[i] = new Vector2(1, 0);
                    break;
                case 2:
                    uvs[i] = new Vector2(0, 1);
                    break;

            }
        }

        mesh.Clear();
        mesh.vertices = vertices3D;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }

    private void RecalculateVoronoiMesh(){ 
        for (int i = 0; i < transform.childCount; i++){
            Transform p = transform.GetChild(i);
            List<Vector2> voronoi = triangulation.GetVoronoiTrangles(i);

            MeshFilter mf = p.Find("Voronoi").GetComponent<MeshFilter>();
            Mesh mesh = mf.mesh;

            int n = voronoi.Count;
            int[] triangles = new int[n];
            Vector3[] vertices = new Vector3[n];
            Vector2[] uvs = new Vector2[n];

            for (int j = 0; j < voronoi.Count; j++) {
                vertices[j] = voronoi[j] - p.position.xy();
                triangles[j] = j;
                switch (j % 3) {
                    case 0:
                        uvs[j] = new Vector2(0, 0);
                        break;
                    case 1:
                        uvs[j] = new Vector2(1, 0);
                        break;
                    case 2:
                        uvs[j] = new Vector2(0, 1);
                        break;
                    
                }
            }

            mesh.Clear();
            mesh.vertices = vertices;
            mesh.uv = uvs;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
        }

    }

    private Transform GetPoint(Vector2 point)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform p = transform.GetChild(i);
            Collider2D collider = p.GetComponent<Collider2D>();
            if (collider.OverlapPoint(point))
            {
                return p;
            }
        }
        return null;
    }
    /*
        public void OnDrawGizmos() {
            if (triangulation != null) {
                Gizmos.color = Color.yellow;
                triangulation.DrawGizmo();

                Vector2 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Triangulation.Triangle t = triangulation.Contains(p);

                if (t!= null) {
                    for (int i = 0; i < 3; i++) {
                        if (t.neighbours[i] != null) {
                            Gizmos.color = Color.green;
                            t.neighbours[i].DrawGizmo();                        
                        }
                    }                

                    Gizmos.color = Color.red;
                    t.DrawGizmo();
                }
            }
        }
    */
}