﻿Shader "Unlit/WireframeShader"
{
    Properties
    {
        _FillColor ("Fill colour", Color) = (0,0,0,0)
        _WireColor ("Wire colour", Color) = (1,1,1,1)
        _Width ("Wire width", Float) = 0.01
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" "IgnoreProjector"="True" }
        LOD 100

        Pass
        {
            // inside Pass
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            fixed4 _FillColor;
            fixed4 _WireColor;
            float _Width;

            float edgeFactor(float3 b){
                float3 d = fwidth(b);
                float3 a3 = smoothstep(float3(0,0,0), d*_Width, b);
                return min(min(a3.x, a3.y), a3.z);
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed3 b = { i.uv.x, i.uv.y, 1-i.uv.x-i.uv.y };
                // fixed4 col = lerp(edgeFactor(b), _FillColor, _WireColor);
                float e = edgeFactor(b);
                fixed4 col = lerp(_WireColor, _FillColor, e);

                // col = e;

                // col = fixed4(barycentric, 1);
                return col;
            }
            ENDCG
        }
    }
}
